<?php

namespace App\Controller;

use App\Entity\Score;
use App\Repository\ScoreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Serializer; // pour convertir la collection de données en json (après l'avoir "normalisée")
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer; //pour "normaliser" la collection de données, cad la convertir en tableau
use Symfony\Component\Serializer\SerializerInterface;

class ApiController extends AbstractController
{

    //page de base
    #[Route('/', name: 'api')]
    public function index(): Response
    {
        return $this->render('api/index.html.twig', [
            'controller_name' => 'ApiController',
        ]);
    }


    //page pour afficher liste de tous les scores
    #[Route('/score/liste', name: 'liste',  methods:'GET')]
    public function liste(ScoreRepository $scoreRepo, SerializerInterface $serializer) // méthode qui va récupérer avec le GET la liste de tous les scores
    {

        //on récup la liste de tous les scores
        $scores = $scoreRepo->findAll();

        // //on normalise (on transforme objet en tableau associatif)
        // $scoresNormalize = $normalizer->normalize($scores, null, ['groups'=> 'score:read']); //on demande à la fonction normalize de normaliser $scores (de transformer objet en tableau associatif), et on lui passe en parametre le groups pour qu'elle aille cherche dans entity ce dont on a besoin
        // dd($scoresNormalize);

        // //on encode (on transforme tableau associatif simple en du texte)  et normalisation + encodage = sérialisation
        // $json = json_encode($scoresNormalize); // transorme en json le tableau d'objets scoresnormalize

        $json = $serializer->serialize($scores, 'json', ['groups'=> 'score:read']); //serialisation, qui remplace lignes précédentes
        
        // On instancie la réponse
        // $response = new Response($json, 200, [
        //     "Content-Type" => "application/json" //content-type permet d'expliquer au navigateur ou postman que la reponse contient du json
        // ]);

        $response = new JsonResponse($json, 200, [], true); // remplace ligne au dessus

        // On envoie la réponse
        return $response;
        }


 
    //pour afficher un score selon son ID
    #[Route('/score/liste/{id}', name: 'score',  methods:'GET')] // pour récupérer 1seul score et pas toute la liste, grâce à l'ID que l'on fait passer dans l'URL
    public function lireScore(ScoreRepository $scoreRepo, int $id, SerializerInterface $serializer) //va chercher le repo car ne fonctionne pas avec entity
    {
        $score = $scoreRepo->find($id); // fait correspondre le repo avec $score, et lui demande d'aller chercher l'id = technique à faire lorsque pb de 
        $json = $serializer->serialize($score, 'json', ['groups'=> 'score:read'] );
        $response = new JsonResponse($json, 200, [], true);
        return $response;
    }
    

    //pour ajouter un score
    #[Route('/score/liste', name:'ajouter', methods:'POST')]
    public function addScore(Request $request, SerializerInterface $serializer, EntityManagerInterface $manager)
    {
    
        $jsonscore = $request->getContent(); //on recup le json de la requete
        
        $score = $serializer->deserialize($jsonscore, Score::class, 'json'); //on deserialize car inverse de la serialisation, afin de transformer du json en objet
        


        /*
        
        $jsonObj = json_decode($jsonscore);
        $score = new Score();
        $score->setScoreGame( $jsonObj->score_game);
        $score->setPseudoGame( $jsonObj->pseudo_game);
        
        */

        dd($jsonscore, $score);

        $manager->persist($score); //prepare 
        $manager->flush(); //enregistre dans bdd
        
    }
    
}
