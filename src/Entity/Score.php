<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ScoreRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ScoreRepository::class)
 */
#[ApiResource]
class Score
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("score:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("score:read")
     */
    private $score_game;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("score:read")
     */
    private $pseudo_game;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScoreGame(): ?string
    {
        return $this->score_game;
    }

    public function setScoreGame(string $score_game): self
    {
        $this->score_game = $score_game;

        return $this;
    }

    public function getPseudoGame(): ?string
    {
        return $this->pseudo_game;
    }

    public function setPseudoGame(string $pseudo_game): self
    {
        $this->pseudo_game = $pseudo_game;

        return $this;
    }
}
